#Start from this image
FROM atlas/analysistop:21.2.98
#copy the repo root in to /code
COPY . /code
#Permanently change workdirectory "\ make sure its a line break"
WORKDIR /code
RUN source ~/release_setup.sh && \
    sudo chown -R atlas /code && \
    pip install numpy && \
    python test.py
